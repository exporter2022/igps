package simulator;

import java.util.Dictionary;
import java.util.Hashtable;

public class Pump {
    private int currentBS;
    double currentInsulinRS = 100;
    double currentGlucagonRS = 100;
    int insulinInjectionUnit = 0;
    int glucagonInjectionUnit = 0;
    public enum PumpModes {
        AUTOMATIC,
        MANUAL
    };
    public enum InjectionType {
        INSULIN,
        GLUCAGON
    };
    final int LOW_BS_THRESHOLD = 90;
    final int HIGH_BS_THRESHOLD = 120;
    final double LOW_IR_THRESHOLD = 50;
    final double LOW_GR_THRESHOLD = 50;
    boolean insulinFlag = false; //set to true when bs levels are too high
    boolean glucagonFlag = false; //set to true when bs levels are too low
    private int insulinCounter = 0, spikeCount = 0, stablCount = 0;
    private int offset = 0, spikeOff = 0, stablOff = 0;
    private PumpModes currentMode;


    public Dictionary getCurrentSettings() {
        Dictionary data = new Hashtable();
        data.put("mode", this.currentMode.toString());
        if(Integer.valueOf(this.currentBS) > 0) {
            data.put("iBS", this.currentBS);
        } else {
            data.put("iBS", this.generateBS());
        }

        return data;
    }

    int injectInsulin(){

        this.currentBS = this.generateBS(currentBS, InjectionType.INSULIN);
        return this.currentBS;
    }

    int injectGlucagon(){
        this.currentBS = this.generateBS(currentBS, InjectionType.GLUCAGON);
        return this.currentBS;
    }

    //this method will only be invoked for very first BS measurement if nothing found in db
    private int generateBS(){
        return (int)(Math.random()*151) + 50;
    }

    //@TODO: need to implement with proper code
    //overloading previous method. Used for any subsequent BS measurement
    private int generateBS(int prevBS){
       return this.calcluateSugarLevel(prevBS);
    }

    private int generateBS(int prevBS, InjectionType type){
        if(type.equals(InjectionType.INSULIN)) {
            this.offset = -24;
            insulinCounter = 4;
            System.out.println("Insulin Injection required");
        }

        if(type.equals(InjectionType.GLUCAGON)) {
            System.out.println("Glucagon Injection required");
            this.stablCount = 5; //affects over 5 cycles
            this.currentBS += 10; //initial offset, since usually it doesn't take 30 minutes for food to affect blood sugar
            this.stablOff = 20; //can be modified depending on standard serving size
        }

        return this.calcluateSugarLevel(prevBS);
    }

    private int calcluateSugarLevel(int prevBS) {
        int bs;
        //======================= calculate new random blood sugar level =============
        //variable just serves to create a probability distribution of how much next BS value will deviate
        int dist = (int)(Math.random()*99);
        //90% probability of next BS being normal fluctuation:
        //{previous BS+(x | x<=25), previous BS-(x | x<=20)}
        if(dist<35)
            bs = (int)(Math.random()*25)+prevBS+offset+stablOff+spikeOff; //increasing => person ate something
        else if(dist<90)
            bs = prevBS-(int)(Math.random()*20)+offset+stablOff+spikeOff; //decrease due to normal bodily function
        else if(dist<97)
            bs = (int)(int)(Math.random()*35)+prevBS+offset+stablOff+spikeOff; //person ate a lot/ food with high glycemic index
        else
            bs = prevBS-(int)(Math.random()*25)+offset+stablOff+spikeOff; //person may have exercised
        //===================== end of new blood sugar calculation ===================

        //if they consumed a stabilizer within the last 5 cycles
        if(stablCount!=0){
            stablCount--;
            if(stablCount==0)
                this.stablOff=0;
            else if(stablCount>3) //initially, more sugar gets absorbed into bloodstream
                this.stablOff*=1.5;
            else //after a while, sugar left decreases and so does amount absorbed
                this.stablOff/=2;
        }

        if(insulinCounter != 0){
            insulinCounter--;
            if(insulinCounter==0)
                this.offset=0;
            //offset decreases as blood insulin levels decrease
            this.offset/=2;
        }
        return bs;
    }



    //generic method to get insulin levels and create new data point
    public int getBS(boolean createsNew){
        if(createsNew)
            this.currentBS = this.generateBS(this.currentBS);
        return this.currentBS;
    }

    // getters and setters
    public void setCurrentMode(PumpModes currentMode) {
        this.currentMode = currentMode;
    }

    public PumpModes getCurrentMode() {
        return currentMode;
    }

    public void setCurrentBS(Integer currentBS) {
        this.currentBS = currentBS;
    }

    public Integer getCurrentBS() {
        return currentBS;
    }

    public Number getCurrentGlucagonRS() {
        return currentGlucagonRS;
    }



}
