package simulator;

import com.mongodb.DBObject;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.text.SimpleDateFormat;
import java.util.*;

public class Controller {
    ObservableList<String> modeList = FXCollections.observableArrayList(Pump.PumpModes.AUTOMATIC.toString(), Pump.PumpModes.MANUAL.toString());
    Model dbModel;
    Pump pump;
    XYChart.Series<String, Number> series = new XYChart.Series<String, Number>();
    int seriesDataIndex = 0;
    public Controller() {
        dbModel = new Model();
        pump = new Pump();
    }

    @FXML
    private ComboBox<String> settingsMode;

    @FXML
    private LineChart<String, Number> lineChart;

    @FXML
    private TextField settingsIBS;

    @FXML
    private Button changeBSBtn;

    @FXML
    private TextField BSInput;

    @FXML
    private TextField manualInput;

    @FXML
    private Label glucagonRSDisplay;

    @FXML
    private Label insulinRSDisplay;

    @FXML
    private Pane manualInjectorPanel;

    @FXML
    private Button refillInsulinRSBtn;

    @FXML
    private Button refillGlucagonRSBtn;

    @FXML
    private Button injectInsulinBtn;

    @FXML
    private Button injectGlucagonBtn;

    @FXML
    private Button settingsSaveButton;

    @FXML
    private Label currentBSL;

    @FXML
    private Label lowBSMessage;

    @FXML
    private TextArea activityLogDisplay;

    @FXML
    private AnchorPane navList;

    @FXML
    private void initialize() {
        settingsMode.setItems(modeList);
        DBObject settings = dbModel.getSettings();
        settingsMode.setValue(settings.get("mode").toString());
        pump.setCurrentBS(Integer.valueOf(settings.get("iBS").toString()));
        pump.setCurrentMode(Pump.PumpModes.valueOf(settings.get("mode").toString().toUpperCase()));
        this.updateUI();
        settingsIBS.setText(settings.get("iBS").toString());
        startSimulationWithRandom();
    }

    private void updateUI() {
        if(pump.getCurrentMode().equals(Pump.PumpModes.AUTOMATIC)) {
            manualInjectorPanel.setDisable(true);
        } else {
            manualInjectorPanel.setDisable(false);
        }

        currentBSL.setText(pump.getCurrentBS().toString());
    }

    @SuppressWarnings("unchecked")
    public void startSimulationWithRandom() {
        lineChart.getData().addAll(series);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run(){
                Platform.runLater(new Runnable(){
                    public void run(){
                        if(pump.insulinFlag){
                            pump.currentInsulinRS = pump.currentInsulinRS - pump.insulinInjectionUnit;
                            // update insulin reservoir text
                            insulinRSDisplay.setText(Double.toString(pump.currentInsulinRS) + " %");
                            // add log to logs
                            writeLogs(String.format("\n%d unit of Insulin injected", pump.insulinInjectionUnit));
                            pump.injectInsulin();
                            pump.insulinFlag=false;
                        }

                        if(pump.glucagonFlag){
                            pump.currentGlucagonRS = pump.currentGlucagonRS - pump.glucagonInjectionUnit;
                            glucagonRSDisplay.setText(Double.toString(pump.currentGlucagonRS) + " %");
                            writeLogs(String.format("\n%d unit of Glucagon injected", pump.glucagonInjectionUnit));
                            pump.injectGlucagon();
                            pump.glucagonFlag=false;
                        }



                        currentBSL.setText(Integer.toString(pump.getBS(true)));
                        Dictionary bsRecord = new Hashtable();
                        bsRecord.put("bs", pump.getBS(false));
                        dbModel.addNewBSRecord(bsRecord);

                        // update the reservoirs
                        insulinRSDisplay.setText(Double.toString(pump.currentInsulinRS) + " %");
                        glucagonRSDisplay.setText(Double.toString(pump.currentGlucagonRS) + " %");

                        // handle low blood sugar
                        if(pump.getBS(false)  <= pump.LOW_BS_THRESHOLD) {
                            //TODO: handle glucagon properly
                            // show warning
                            lowBSMessage.setVisible(true);
                            if(pump.getCurrentMode().equals(Pump.PumpModes.AUTOMATIC)) {
                                pump.glucagonFlag = true;
                                pump.glucagonInjectionUnit = 5;
                            } else {
                                writeLogs(String.format("\nNeed to inject glucagon"));
                            }

                        } else {
                            lowBSMessage.setVisible(false);
                        }


                        // handle higher blood sugar
                        if(pump.getBS(false) > pump.HIGH_BS_THRESHOLD) {
                            if(pump.getCurrentMode().equals(Pump.PumpModes.AUTOMATIC)) {
                                //TODO: handle insulin inject properly
                                pump.insulinFlag = true;
                                //TODO: need to make it dynamic based on calculation how much it needs to inject
                                pump.insulinInjectionUnit = 5;
                            } else {
                                writeLogs(String.format("\nNeed to inject insulin"));
                            }

                        }

                        addNewDataToChart();

                        // show warning if insulin level below lower threshold
                        System.out.println("Current insulin reservoir is " + Double.toString(pump.currentInsulinRS));
                        if(pump.currentInsulinRS < pump.LOW_IR_THRESHOLD) {
                            insulinRSDisplay.setTextFill(Color.RED);
                        } else {
                            insulinRSDisplay.setTextFill(Color.BLACK);
                        }

                        // show warning if glucagon level below lower threshold
                        if(pump.currentGlucagonRS < pump.LOW_GR_THRESHOLD) {
                            glucagonRSDisplay.setTextFill(Color.RED);
                        } else {
                            glucagonRSDisplay.setTextFill(Color.BLACK);
                        }

                        // set the handler for refill insulin
                        refillInsulinRSBtn.setOnAction(new EventHandler<ActionEvent>() {
                            @Override public void handle(ActionEvent e) {
                                System.out.println("calling thread one");
                                pump.currentInsulinRS = 100;
                                insulinRSDisplay.setText(pump.currentInsulinRS + " %");
                                insulinRSDisplay.setTextFill(Color.BLACK);
                            }
                        });

                        // set the handler for refill glucagon
                        refillGlucagonRSBtn.setOnAction(new EventHandler<ActionEvent>() {
                            @Override public void handle(ActionEvent e) {
                                System.out.println("calling thread one");
                                pump.currentGlucagonRS = 100;
                                glucagonRSDisplay.setText(pump.currentGlucagonRS + " %");
                                glucagonRSDisplay.setTextFill(Color.BLACK);
                            }
                        });

                        // set the handler for change blood sugar level
                        changeBSBtn.setOnAction(new EventHandler<ActionEvent>() {
                            @Override public void handle(ActionEvent e) {
                                pump.setCurrentBS(Integer.valueOf(BSInput.getText()));
                                updateUI();
                                BSInput.setText("0");
                            }
                        });

                        // set the handler to manually inject insulin
                        injectInsulinBtn.setOnAction(new EventHandler<ActionEvent>() {
                            @Override public void handle(ActionEvent e) {
                                pump.insulinInjectionUnit = Integer.valueOf(manualInput.getText());
                                pump.insulinFlag = true;
                                pump.injectInsulin();
                                manualInput.setText("0");
                                pump.currentInsulinRS = pump.currentInsulinRS - pump.insulinInjectionUnit;
                                // update insulin reservoir text
                                insulinRSDisplay.setText(Double.toString(pump.currentInsulinRS) + " %");
                                // add log to logs
                                writeLogs(String.format("\n%d unit of Insulin injected", pump.insulinInjectionUnit));
                                pump.insulinFlag = false;
                                addNewDataToChart();
                                updateUI();
                            }
                        });

                        // set the handler to manually inject glucagon
                        injectGlucagonBtn.setOnAction(new EventHandler<ActionEvent>() {
                            @Override public void handle(ActionEvent e) {
                                pump.glucagonInjectionUnit = Integer.valueOf(manualInput.getText());
                                pump.glucagonFlag = true;
                                manualInput.setText("0");
                                pump.currentGlucagonRS = pump.currentGlucagonRS - pump.glucagonInjectionUnit;
                                glucagonRSDisplay.setText(Double.toString(pump.currentGlucagonRS) + " %");
                                writeLogs(String.format("\n%d unit of Glucagon injected", pump.glucagonInjectionUnit));
                                pump.injectGlucagon();
                                pump.glucagonFlag=false;
                                addNewDataToChart();
                                updateUI();
                            }
                        });

                    }
                });
            }
        },1000,15000);
    }

    void addNewDataToChart() {
        Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("hh:mm:ss a");
        series.getData().add(new LineChart.Data<String,Number>(ft.format(dNow), pump.getBS(false)));
        seriesDataIndex++;
    }

    @FXML
    void saveSettings(ActionEvent event) {
        pump.setCurrentMode(Pump.PumpModes.valueOf(settingsMode.getValue().toUpperCase()));
        pump.setCurrentBS(Integer.valueOf(settingsIBS.getText()));
        dbModel.updateSettings(pump.getCurrentSettings());
        this.updateUI();
        System.out.println("settings are saved insulin dost %S");
        System.out.println(settingsIBS.getText() +  settingsMode.getValue());
    }

    @FXML
    void changeBSLevel(ActionEvent event) {
        pump.setCurrentBS(Integer.valueOf(BSInput.getText()));
        this.updateUI();
        BSInput.setText("0");
    }


    @FXML
    void refillGlucagonRS(ActionEvent event) {
        pump.currentGlucagonRS = 100;
        insulinRSDisplay.setText(pump.currentGlucagonRS + "%");
    }

    @FXML
    void refillInsulinRS(ActionEvent event) {
        pump.currentInsulinRS = 100;
        insulinRSDisplay.setText(pump.currentInsulinRS + "%");
    }

    void writeLogs(String message) {
        activityLogDisplay.appendText(message);
    }

}