package simulator;

import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.DBCollectionUpdateOptions;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;

import java.lang.reflect.Array;
import java.util.*;

public class Model {
    MongoClient client = new MongoClient("localhost", 27017);
    DB db = client.getDB("igps");
    public DBObject getSettings() {
        DBCollection settings = db.getCollection("settings");
        DBCursor cursor = settings.find();
        DBObject doc = cursor.one();
        System.out.println(doc.get("mode"));
        return doc;
    }

    public void updateSettings(Dictionary data) {
        BasicDBObject query = new BasicDBObject();
        query.put("name", "meta");

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.put("name", "meta");
        newDocument.put("iBS", data.get("iBS"));
        newDocument.put("mode", data.get("mode"));

        BasicDBObject updateObject = new BasicDBObject();
        updateObject.put("$set", newDocument);

        DBCollection settings = db.getCollection("settings");
        settings.update(query, updateObject, (new DBCollectionUpdateOptions()).upsert(true));
        System.out.println("we are done with update");
    }

    public void addNewBSRecord(Dictionary data) {
        //@TODO: store the blood sugar record to related collection
        System.out.println("saved new blood sugar record");
    }
}
