package simulator;


import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.net.URL;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        try {
            URL url = new File("src/main/java/simulator/simulator.fxml").toURL();
            Parent root = FXMLLoader.load(url);
            primaryStage.setTitle("Simulator");
            primaryStage.setScene(new Scene(root, 700, 700));
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // just to make sure program is closing on event
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent t) {
                // we can do the additional cleanup in here
                System.out.println("Closing program");
                Platform.setImplicitExit(true);
                System.exit(0);
            }
        });
    }


    public static void main(String[] args) {
        launch(args);
    }
}
